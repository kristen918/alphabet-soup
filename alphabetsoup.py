'''
Kristen Smith
kristen918@gmail.com

Alphabet Soup

Filename:               alphabetsoup.py
Example use:            python alphabetsoup.py sample.txt    
Runtime environment:    Python 3.9.1
Package manager:        PIP

Sample input:
5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE

Result (print to screen):
HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1

'''

import sys
import timeit

def main(argv):
    if len(argv) != 2 or (len(argv) == 2 and argv[1] == '-h'):
        print('%s <inputfile>'%(argv[0]))
    else:
        alphabetsoup(argv[1])

def alphabetsoup(filename):
    '''Finds start and end position of words in a word search grid.
    Required: filename (string) - name of file containing required input (size of grid, grid, and word list).
    '''
    start = timeit.default_timer()  
    size = 0
    grid = []
    words = ()
    results = {}
    
    with open(filename) as f:
        data = f.readlines()
        size = int(data[0].split('x')[0])
        for i in range(size):
            grid.append(data[i+1].strip().split(' '))
        words = tuple([word.strip() for word in data[size+1:] if len(word.strip()) > 0])

    for word in words:
        results[word] = '<<word not found>>'
        word_to_check = word.replace(' ', '')
        length = len(word_to_check)
        match = False
        for row_num, row in enumerate(grid):
            for col_num, letter in enumerate(row):
                if match is False and letter == word[0]:
                    ends = get_directions(length, size, row_num, col_num)
                    for end in ends:
                        word_to_match = ''
                        for i in range(length):
                            word_to_match += grid[row_num + end[0]*i][col_num + end[1]*i]
                        if word_to_check == word_to_match:
                            results[word] = '%d:%d %d:%d'%(row_num, col_num, row_num + end[0]*(length-1), col_num + end[1]*(length-1))
                            match = True
                            break
    
    for word in words:
        print('%s %s'%(word, results[word]))
    
    stop = timeit.default_timer()

    print('Total runtime: %s seconds'%(round(stop - start, 4)))

def get_directions(length, grid_size, row, col):
    '''Returns cardinal directions of available arrays given a starting point, length of the array, and the size of the grid. 
    Required: length (int) - number of elements in the array to check.
    Required: grid_size (int) - length of one side of the square grid.
    Required: row (int) - starting row index.
    Required: col (int) - starting column index.
    Returns: array of arrays of size 2 of values 0, 1, -1 corresponding to the change in the [row, column] index.
        E.g. [[0, 1], [-1, -1]] corresponds to [East, Northwest].
    '''
    result = []
    if all([isinstance(i, int) for i in [length, grid_size, row, col]]):
        # vertical
        if length <= row + 1:
            result.append([-1, 0])
        if length <= grid_size - row:
            result.append([1, 0])

        # horizontal
        if length <= grid_size - col:
            result.append([0, 1])
        if length <= col + 1:
            result.append([0, -1])

        # diagonal
        for [i, j] in result:
            for [k, l] in result:
                if i != k and j != l:
                    to_add = [i+k, j+l]
                    if to_add not in result:
                        result.append(to_add)

    return(result)

if __name__ == "__main__":
    main(sys.argv)